# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Instructions:
---------------

- Edit the included credentials-skel.ini, saving the result as credentials.ini and placing it in the hello folder.

- Use python 3 to run hello.py and receive the message you have sent yourself.

## Authors
---------------

### Original Version:
* **UOCIS322** [Proj0-Hello](https://bitbucket.org/UOCIS322/proj0-hello/)

### Rad Updates to README.md:

* **Cole Sabin** [E-Mail](mailto:csabin@uoregon.edu)
